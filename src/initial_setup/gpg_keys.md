# GPG Keys Setup

## Troubleshooting GPG Keys
If you ever run into issues related to keys while using pacman, AUR etc, try these
 - ### Switch to a different keyserver
   You can prioritize other keyservers, `hkp://keyserver.ubuntu.com` is great.  

   Edit/Create
      ```
      ~/.gnupg/gpg.conf
      ```
      and
      ```
      /etc/pacman.d/gnupg/gpg.conf
      ```


      Comment out other ones and add your one by adding `keyserver <your-key-server>`
      Afterwards they should look something like this
      ```
      keyserver hkp://keyserver.ubuntu.com
      #keyserver hkp://pool.sks-keyservers.net
      ```
      and
      ```
      no-greeting
      no-permission-warning
      lock-never

      keyserver hkp://keyserver.ubuntu.com
      #keyserver hkp://pool.sks-keyservers.net

      keyserver-options timeout=10
      keyserver-options import-clean
      keyserver-options no-self-sigs-only
      ```
 - ### Receive keys
   You might get failures with a specific key, errors will show the key.
   You can try to receive the key by running
   ```sh
   gpg --recv-keys <key-you-need>
   ```
   > It might fail if the keyserver is having problems.
   > In that case you can try switching to a different keyserver with the steps above.
   > Or you can override it here.
   > ```sh
   > gpg --keyserver hkp://keyserver.ubuntu.com --recv-keys <key-you-need>
   > ```
### For further reference: [GnuPG - ArchWiki](https://wiki.archlinux.org/index.php/GnuPG)
