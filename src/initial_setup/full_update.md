# Full Update

### Now it's time to finally do a full update on your system.  
### Your options are
 - ### Use the 'pamac' GUI app, which is listed as 'Add/Remove Programs' in manjaro.
   1. Go into the app
   2. Go to the Updates tab
   3. Hit Apply
 - ### Use 'pacman'
   Run the following command
   ```sh
   sudo pacman -Syyu
   ```

### You may also want to Update the Kernel
1. Search Kernel from start menu
2. Hit install on the one you want
3. Reboot to apply