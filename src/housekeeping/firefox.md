# Firefox

Now to clean up our beloved Firefox browser
1. Change the Firefox home from the manjaro page to the default page (Search `Home` in settings)
2. Log into Firefox Account
3. Setup LastPass
4. Set default zoom to 150% (Search `Zoom` in settings)