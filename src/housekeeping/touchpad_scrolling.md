# Fix Touchpad Scroll
By default, Manjaro has scrolling set to the opposite of what we're used to.
To fix this
- Go to `System Settings > Input Devices > Touchpad`
- Select `Invert Scroll Direction (Natural Scrolling)`
- Hit `Apply`