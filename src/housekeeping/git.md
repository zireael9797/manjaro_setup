# Git

To set up git to be used over ssh
1. Run `ssh-keygen` to generate ssh keys
2. You will be prompted to select a directory for the new key, pick the default (`home/username/.ssh/id_rsa`)
3. Hit enter through the steps
4. Open `home/username/.ssh/id_rsa.pub` and copy it's contents
5. `Gitlab` > ProfilePhoto > Settings > SSH Keys > Paste key, Set Title, Hit `Add key`
6. `Github` > ProfilePhoto > Settings > SSH and GPG Keys > `Create New SSH Key`, Paste key, Give it a name
