# Summary

- [Manjaro Installation Steps](./installation.md)
- [Initial Setup](initial_setup/README.md)
    - [Mirrors](initial_setup/mirrors.md)
    - [Pacman Signing](initial_setup/pacman_signing.md)
    - [GPG Keys Setup](initial_setup/gpg_keys.md)
    - [Full Update](initial_setup/full_update.md)
- [Housekeeping](housekeeping/README.md)
    - [Fix Touchpad Scroll](housekeeping/touchpad_scrolling.md)
    - [Firefox](housekeeping/firefox.md)
    - [Git](housekeeping/git.md)
